export class UserProfileFormModel{
    firstName: string;
    lastName: string;
    age: number;
    email : string;
    termsConditions: string;
    persontype:string;
    technology:string;
}